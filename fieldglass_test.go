package fieldglass_test

import (
	"gitlab.com/high-creek-software/fieldglass"
	"log"
	"testing"
)

const (
	spacexEndpoint    = "https://api.spacex.land/graphql"            // https://studio.apollographql.com/public/SpaceX-pxxbxen/home?variant=current
	countriesEndpoint = "https://countries.trevorblades.com/graphql" //https://studio.apollographql.com/public/countries/home?variant=current
)

func TestFieldGlass(t *testing.T) {
	fg := fieldglass.NewFieldGlass()
	schema, err := fg.Load(spacexEndpoint)
	if err != nil {
		t.Fatal(err)
	}
	log.Println(schema)
}
