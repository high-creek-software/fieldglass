package fieldglass

import (
	"encoding/json"
	"strings"
)

type Directive struct {
	Name         string              `json:"name"`
	Description  *string             `json:"description,omitempty"`
	Locations    []DirectiveLocation `json:"locations"`
	Args         []InputValue        `json:"args"`
	IsRepeatable bool                `json:"isRepeatable"`
}

type DirectiveLocation int

var directiveLocations = []string{"QUERY", "MUTATION", "SUBSCRIPTION", "FIELD", "FRAGMENT_DEFINITION", "FRAGMENT_SPREAD",
	"INLINE_FRAGMENT", "VARIABLE_DEFINITION", "SCHEMA", "SCALAR", "OBJECT", "FIELD_DEFINITION", "ARGUMENT_DEFINITION",
	"INTERFACE", "UNION", "ENUM", "ENUM_VALUE", "INPUT_OBJECT", "INPUT_FIELD_DEFINITION", "UNKNOWN"}

const (
	DirectiveLocationQuery DirectiveLocation = iota
	DirectiveLocationMutation
	DirectiveLocationSubscription
	DirectiveLocationField
	DirectiveLocationFragmentDefinition
	DirectiveLocationFragmentSpread
	DirectiveLocationInlineFragment
	DirectiveLocationVariableDefinition
	DirectiveLocationSchema
	DirectiveLocationScalar
	DirectiveLocationObject
	DirectiveLocationFieldDefinition
	DirectiveLocationArgumentDefinition
	DirectiveLocationInterface
	DirectiveLocationUnion
	DirectiveLocationEnum
	DirectiveLocationEnumValue
	DirectiveLocationInputObject
	DirectiveLocationInputFieldDefinition
	DirectiveLocationUnknown
)

func (dl DirectiveLocation) String() string {
	return directiveLocations[dl]
}
func DirectiveLocationFromString(dl string) DirectiveLocation {
	for i, elem := range directiveLocations {
		if elem == dl {
			return DirectiveLocation(i)
		}
	}
	return DirectiveLocationUnknown
}

func (dl *DirectiveLocation) UnmarshalJSON(b []byte) error {
	blStr := strings.Trim(string(b), `"`)
	*dl = DirectiveLocationFromString(blStr)
	return nil
}

func (dl DirectiveLocation) MarshalJSON() ([]byte, error) {
	return json.Marshal(dl.String())
}
