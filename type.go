package fieldglass

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Type struct {
	Kind          TypeKind     `json:"kind"`
	Name          *string      `json:"name,omitempty"`
	Description   *string      `json:"description,omitempty"`
	Fields        []Field      `json:"fields,omitempty"`
	Interfaces    []Type       `json:"interfaces,omitempty"`
	PossibleTypes []Type       `json:"possibleTypes,omitempty"`
	EnumValues    []EnumValue  `json:"enumValues,omitempty"`
	InputFields   []InputValue `json:"inputFields,omitempty"`
	OfType        *Type        `json:"ofType,omitempty"`
}

func (t Type) FormatName() string {
	if t.Name != nil {
		return *t.Name
	}

	if t.OfType != nil {
		childName := t.OfType.FormatName()
		if t.Kind == TypeKindList {
			return fmt.Sprintf("[%s]", childName)
		} else if t.Kind == TypeKindNonNull {
			return fmt.Sprintf("%s!", childName)
		}
		return childName
	}

	return "unknown"
}

func (t Type) RootName() string {
	if t.Name != nil {
		return *t.Name
	}

	if t.OfType != nil {
		return t.OfType.RootName()
	}
	return "Unknown"
}

func (t Type) RootType() TypeKind {
	if t.OfType != nil {
		return t.OfType.RootType()
	}

	return t.Kind
}

var builtInScalars = map[string]bool{"Int": true, "Float": true, "String": true, "Boolean": true, "ID": true}

func (t Type) IsBuiltin() bool {
	return strings.HasPrefix(*t.Name, "__") || builtInScalars[*t.Name]
}

type TypeKind int

var typeStrings = []string{"SCALAR", "OBJECT", "INTERFACE", "UNION", "ENUM", "INPUT_OBJECT", "LIST", "NON_NULL", "UNKNOWN"}

const (
	TypeKindScalar TypeKind = iota
	TypeKindObject
	TypeKindInterface
	TypeKindUnion
	TypeKindEnum
	TypeKindInputObject
	TypeKindList
	TypeKindNonNull
	TypeKindUnknown
)

func (tk TypeKind) String() string {
	return typeStrings[tk]
}

func typeKindFromString(tk string) TypeKind {
	for i, elem := range typeStrings {
		if elem == tk {
			return TypeKind(i)
		}
	}
	return TypeKindUnknown
}

func (tk *TypeKind) UnmarshalJSON(b []byte) error {
	tkStr := strings.Trim(string(b), `"`)
	*tk = typeKindFromString(tkStr)
	return nil
}

func (tk TypeKind) MarshalJSON() ([]byte, error) {
	return json.Marshal(tk.String())
}
