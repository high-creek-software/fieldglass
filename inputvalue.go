package fieldglass

type InputValue struct {
	Name         string  `json:"name"`
	Description  *string `json:"description,omitempty"`
	Type         *Type   `json:"type"`
	DefaultValue *string `json:"defaultValue,omitempty"`
}
