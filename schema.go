package fieldglass

import (
	"errors"
	"strings"
)

var ErrorQueryNotFound = errors.New("query not found")
var ErrorMutationNotFound = errors.New("mutation not found")

type payload struct {
	Data data `json:"data"`
}

type data struct {
	Schema Schema `json:"__schema"`
}

type Schema struct {
	Types            []Type      `json:"types"`
	QueryType        *Type       `json:"queryType"`
	MutationType     *Type       `json:"mutationType,omitempty"`
	SubscriptionType *Type       `json:"subscriptionType,omitempty"`
	Directives       []Directive `json:"directives"`
}

func (s Schema) String() string {
	sb := strings.Builder{}
	sb.WriteString("--- Types ---\n")
	for _, t := range s.Types {
		name := ""
		if t.Name != nil {
			name = *t.Name
		}
		sb.WriteString(name + "\n")
	}

	sb.WriteString("\n")
	sb.WriteString("\n")

	sb.WriteString("--- Query Type ---\n")
	for _, q := range s.QueryType.PossibleTypes {
		name := ""
		if q.Name != nil {
			name = *q.Name
		}
		sb.WriteString(name + "\n")
	}

	return sb.String()
}

func (s *Schema) GetQuery() (*Type, error) {
	if s.QueryType.Name == nil {
		return nil, ErrorQueryNotFound
	}

	for _, typ := range s.Types {
		if *typ.Name == *s.QueryType.Name {
			return &typ, nil
		}
	}

	return nil, ErrorQueryNotFound
}

func (s *Schema) HasMutations() bool {
	return s.MutationType != nil
}

func (s *Schema) GetMutation() (*Type, error) {
	if !s.HasMutations() {
		return nil, ErrorMutationNotFound
	}

	for _, typ := range s.Types {
		if *typ.Name == *s.MutationType.Name {
			return &typ, nil
		}
	}

	return nil, ErrorMutationNotFound
}

func (s *Schema) FindType(name string) (*Type, error) {
	for _, elem := range s.Types {
		if elem.Name != nil {
			if name == *elem.Name {
				return &elem, nil
			}
		}
	}
	return nil, nil
}
