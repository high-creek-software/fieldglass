package fieldglass

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

var ErrorUnsuccessful = errors.New("request unsuccessful")

type FieldGlass interface {
	Load(url string) (*Schema, error)
}

func NewFieldGlass(debug bool) FieldGlass {
	return &FieldGlassImpl{debug: debug}
}

type FieldGlassImpl struct {
	debug bool
}

func (f FieldGlassImpl) Load(url string) (*Schema, error) {

	query, err := json.Marshal(graphqlRequest{Query: IntrospectionQuery})
	if err != nil {
		return nil, err
	}

	rdr := bytes.NewReader(query)
	//resp, err := http.Post(url, "application/json", rdr)
	req, err := http.NewRequest(http.MethodPost, url, rdr)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")

	tr := &http.Transport{
		Proxy:                 http.ProxyFromEnvironment,
		IdleConnTimeout:       120 * time.Second,
		TLSHandshakeTimeout:   120 * time.Second,
		ExpectContinueTimeout: 10 * time.Second,
	}

	client := http.Client{
		Transport: tr,
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		log.Println("Response:", string(data))
		return nil, fmt.Errorf("error requesting schema: %w %d", ErrorUnsuccessful, resp.StatusCode)
	}

	if f.debug {
		log.Println(string(data))
	}

	payload := &payload{}
	err = json.Unmarshal(data, payload)

	return &payload.Data.Schema, err
}

type graphqlRequest struct {
	Query string `json:"query,omitempty"`
}
